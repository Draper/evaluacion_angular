export class User{
    constructor(
      public _id: string,
      public name: string,
      public lastname: string,
      public app: string,
      public email: string,
      public image: string,
      public password: string,
    ){}
  }
  