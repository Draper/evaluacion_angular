import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  public user:User;
  public error;
  public messageError;

  constructor(
    private _userService: UserService,
    private _router: Router
  ) { 
    this.error = false;
    this.user = new User('', '', '', 'APP_BCK', '', '', '');
  }

  ngOnInit() {
    this.auth();
  }

  onSubmit(){
    let user_login = {
      email: this.user.email,
      password: this.user.password,
      app: this.user.app
    }
    this._userService.logIn(user_login).subscribe(
      response => {
        localStorage.setItem('sessionTokenBck', response.sessionTokenBck);
        localStorage.setItem('userSesion', JSON.stringify(response));
        this._router.navigate(['/booking']);
      },
      error =>{
        console.log('ERROR', error._body)
        this.error = true;
        this.messageError = error._body;
      }
    )
  }

  auth(){
    let userSesion = localStorage.getItem('userSesion');
    let sessionTokenBck = localStorage.getItem('sessionTokenBck');
    
    if(userSesion == null || userSesion == undefined || sessionTokenBck == undefined || sessionTokenBck == null ){
    }else{
      this._router.navigate(['/booking']);
    }
  }
}
