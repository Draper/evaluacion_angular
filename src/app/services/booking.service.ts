import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class BookingService{
  public identity;
  public token;
  public url: string;

  constructor(private _http: Http){
    this.url = GLOBAL.url;
  }

  listBooking(user, token){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'adminemail': user.email,
      'token': token,
      'app': 'APP_BCK'
    })

    let options = new RequestOptions({headers: headers})

    return this._http.get(this.url + 'user/contacto@tuten.cl/bookings?current=true', options)
                      .map(res => res.json())
  }

  getToken(){
    let token = localStorage.getItem('token')
    if(token != "undefined"){
      this.token = token;
    }else{
      this.token = null;
    }
    return this.token;
  }
}
