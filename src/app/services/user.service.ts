import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class UserService{
  public identity;
  public token;
  public url: string;

  constructor(private _http: Http){
    this.url = GLOBAL.url;
  }

  logIn(user_to_login){
      let parameters_json = {
      }
      let json = JSON.stringify(parameters_json);
      let params = json;
      let headers = new Headers(
        {
          'Content-Type':'application/json',
          'Accept': 'application/json',
          'password': user_to_login.password,
          'app': user_to_login.app,
        }
      );
      return this._http.put(this.url + 'user/' + user_to_login.email, params, {headers: headers})
                        .map(res => res.json());
  }
}
