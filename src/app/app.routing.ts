import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Impor user
import { LoginComponent } from './login/login.component';
import { BookingComponent } from './booking/booking.component';

const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'booking', component: BookingComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', component: LoginComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
