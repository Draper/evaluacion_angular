import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BookingService } from '../services/booking.service';
import { Booking } from '../models/booking';
import { InitBookingDataTable } from 'assets/js/datatable';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
  providers: [BookingService]
})
export class BookingComponent implements OnInit {
  
  public booking: Booking;
  public isLogged: boolean;
  public bookings;

  constructor(
    private _bookingService: BookingService,
    private _router: Router
  ) { 
    this.booking = new Booking('', '', '','', '', '');
    this.isLogged = false;
    this.auth();
  }

  ngOnInit() {
    this.listContacts();
  }

  listContacts():void{
    let userSesion = JSON.parse(localStorage.getItem('userSesion'));
    let sessionTokenBck = localStorage.getItem('sessionTokenBck');

    this._bookingService.listBooking(userSesion, sessionTokenBck).subscribe(
      response => {
        this.bookings = response;
        console.log('RESPONSE', response);
        let bookingsArr = []
        response.forEach(element => {
          bookingsArr.push([element.bookingId, element.tutenUserClient.firstName +' '+ element.tutenUserClient.lastName, element.bookingTime, element.locationId.streetAddress, element.bookingPrice]);
        });
        InitBookingDataTable(bookingsArr)
      },
      error => {
        console.log('ERROR', error);
      }
    ) 
  }

  auth():void{
    let userSesion = localStorage.getItem('userSesion');
    let sessionTokenBck = localStorage.getItem('sessionTokenBck');
    if(userSesion == null || userSesion == undefined || sessionTokenBck == undefined || sessionTokenBck == null){
      this.isLogged = false;
    }else{
      this.isLogged = true;
    }
  }

  logOut():void{
    localStorage.removeItem('userSesion');
    localStorage.removeItem('sessionTokenBck');
    this._router.navigate(['/login']);
  }

}
