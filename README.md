# Prueba técnica Tuten - Angular

## Tecnologias usadas
- Angular 6 (para el desarrollo de la aplicacion)
- npm (para la gestion de paquetes)

## Levantando el proyecto

- Asegúrate de tener el [Angular CLI](https://github.com/angular/angular-cli#installation) instalado globalmente. Usamos [npm](https://npm.com) para gestionar las dependencias, por lo que le recomendamos encarecidamente que la use. Puedes instalarlo desde [aqui](https://www.npmjs.com/get-npm).

- Luego nos ubicamos en la raiz del proyecto y ejecutamos en el terminal `npm install` para descargar las dependencias (tomara unos minutos).

- Posteriormente ejecutamos en el terminal `ng serve`
- Finalmente abrir el navegador y escribir`http://localhost:4200/`. Se apreciaran los resultados de la prueba tecnica.